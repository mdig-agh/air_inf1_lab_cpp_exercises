#ifndef MATLIB_H_
#define MATLIB_H_

// Returns n! (the factorial of n).  For negative n, n! is defined to be 1.
int factorial(int n);

#endif  // MATLIB_H_

