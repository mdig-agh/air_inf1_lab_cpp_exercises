// Dołącz plik nagłówkowy Googletest.
#include "gtest/gtest.h"

// Aby testy zostały uruchomione, Googletest wymaga funkcji
// main() o poniżej zawartości.
int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

