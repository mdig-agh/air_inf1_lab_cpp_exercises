// Dołącz plik nagłówkowy Googletest.
#include "gtest/gtest.h"

// Dołącz plik nagłówkowy własnej biblioteki.
#include "matlib.hpp"

// Przypadek testowy #1:
//   Sprawdź wartość 0! (przypadek szczególny).
TEST(FactorialTest, HandlesZeroInput) {
    // Upewnij się, że obliczona wartość 0! wynosi 1.
    EXPECT_EQ(1, factorial(0));  // EXPECT_EQ() to tzw. asercja (makro testowe)
}

// Przypadek testowy #2:
//   Sprawdź wartość silni dla liczb całkowitych dodatnich.
TEST(FactorialTest, HandlesPositiveInput) {
    EXPECT_EQ(1, factorial(1));
    EXPECT_EQ(2, factorial(2));
    EXPECT_EQ(6, factorial(3));
}

