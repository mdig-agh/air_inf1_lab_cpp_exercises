TEST(MatlabVectorTest, addVectorsOfSameSize) {
    Matlab::Vector v1(std::vector<int>{1, 2});
    Matlab::Vector v2(std::vector<int>{4, 5});

    ASSERT_NO_THROW({
                        Matlab::Vector v_sum = Matlab::add_vectors(v1, v2);

                        ASSERT_EQ(v_sum.size(), 2U);
                        EXPECT_EQ(v_sum[0], 5);
                        EXPECT_EQ(v_sum[1], 7);
                    });
}

TEST(MatlabVectorTest, addVectorsOfDifferentSize) {
    Matlab::Vector v1(std::vector<int>{1});
    Matlab::Vector v2(std::vector<int>{1, 2});

    ASSERT_THROW(add_vectors(v1, v2), std::invalid_argument);
}

TEST(MatlabVectorAuxTest, exceptionCatchingWhenAddingVectorsOfSameSizes) {
    Matlab::Vector v1(std::vector<int> { 1 });
    Matlab::Vector v2(std::vector<int> { 1 });

    EXPECT_EQ(Matlab::was_exception_raised_when_adding_vectors(v1, v2), "");
}

TEST(MatlabVectorAuxTest, exceptionCatchingWhenAddingVectorsOfDifferentSizes) {
    Matlab::Vector v1(std::vector<int> { 1 });
    Matlab::Vector v2(std::vector<int> { 1, 2 });

    EXPECT_EQ(Matlab::was_exception_raised_when_adding_vectors(v1, v2), "Vectors have unequal size (1 and 2)");
}
